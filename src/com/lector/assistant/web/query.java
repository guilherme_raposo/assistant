package com.lector.assistant.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ibm.watson.assistant.v2.model.MessageResponse;
import com.lector.assistant.controller.Instances;
import com.lector.assistant.controller.LivIA;

@WebServlet("/query")
public class query extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");

		LivIA livIA = Instances.getInstance(request.getSession());

		MessageResponse messageResponse;

		try {
			messageResponse = livIA.query(request.getParameter("text"), request.getParameter("accountId"));
		} catch (Exception e) {
			livIA = Instances.replaceInstance(request.getSession());

			messageResponse = livIA.query(request.getParameter("text"), request.getParameter("accountId"));
		}

		PrintWriter pw = response.getWriter();

		response.setContentType("application/json");

		pw.append(messageResponse.toString());
	}
}
