package com.lector.assistant.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;

public class Instances {
	private static Map<String, LivIA> instances = new HashMap<String, LivIA>();

	public static LivIA getInstance(HttpSession session) {
		String sessionId = session.getId();

		if (instances.containsKey(sessionId)) {
			return instances.get(sessionId);
		} else {
			LivIA livia = new LivIA();
			instances.put(sessionId, livia);
			return livia;
		}
	}

	public static LivIA replaceInstance(HttpSession session) {
		String sessionId = session.getId();

		LivIA livia = new LivIA();
		instances.replace(sessionId, livia);

		return livia;
	}

}
