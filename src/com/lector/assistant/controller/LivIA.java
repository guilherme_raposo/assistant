package com.lector.assistant.controller;

import java.util.HashMap;
import java.util.Map;

import com.ibm.cloud.sdk.core.security.IamAuthenticator;
import com.ibm.watson.assistant.v2.Assistant;
import com.ibm.watson.assistant.v2.model.CreateSessionOptions;
import com.ibm.watson.assistant.v2.model.MessageContext;
import com.ibm.watson.assistant.v2.model.MessageContextSkill;
import com.ibm.watson.assistant.v2.model.MessageContextSkills;
import com.ibm.watson.assistant.v2.model.MessageInput;
import com.ibm.watson.assistant.v2.model.MessageInputOptions;
import com.ibm.watson.assistant.v2.model.MessageOptions;
import com.ibm.watson.assistant.v2.model.MessageResponse;
import com.ibm.watson.assistant.v2.model.SessionResponse;
import com.lector.assistant.config.Api;

public class LivIA {
	IamAuthenticator authenticator;
	Assistant assistant;
	CreateSessionOptions options;
	SessionResponse response;

	
	public LivIA() {
		authenticator = new IamAuthenticator(Api.API_KEY);
		assistant = new Assistant(Api.ASSISTANT_VERSION, authenticator);
		assistant.setServiceUrl(Api.SERVICE_URL);
		options = new CreateSessionOptions.Builder(Api.ASSISTANT_ID)
				.build();

		response = assistant.createSession(options).execute().getResult();
	}

	public MessageResponse query(String query, String accountId) {
		
		MessageInputOptions inputOptions = new MessageInputOptions.Builder()
				.returnContext(true)
				.build();

		MessageInput input = new MessageInput.Builder()
				.messageType("text")
				.text(query)
				.options(inputOptions)
				.build();

		MessageOptions options = new MessageOptions.Builder()
				.assistantId(Api.ASSISTANT_ID)
				.sessionId(response.getSessionId())
				.input(input)
				.context(getAccountContext(accountId))
				.build();

		MessageResponse response = assistant
				.message(options)
				.execute()
				.getResult();
		
		return response;
	}

	private MessageContext getAccountContext(String accountId) {
		Map<String, Object> userDefinedContext = new HashMap<>();

		userDefinedContext.put("account_id", accountId + "");

		MessageContextSkill mainSkillContext = new MessageContextSkill.Builder()
				.userDefined(userDefinedContext)
				.build();

		MessageContextSkills skillsContext = new MessageContextSkills();
		skillsContext.put("main skill", mainSkillContext);

		MessageContext context = new MessageContext.Builder()
				.skills(skillsContext)
				.build();

		return context;
	}
}
